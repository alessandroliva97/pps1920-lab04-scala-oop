package u04lab.code

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u04lab.code.Lists.List
import u04lab.code.Lists.List._
import u04lab.code.Optionals.Option
import u04lab.code.Optionals.Option._

class PowerIteratorsTest {

  val factory = new PowerIteratorsFactoryImpl()

  @Test
  def testIncremental(): Unit = {
    val pi = factory.incremental(5,_+2); // pi produce 5,7,9,11,13,...
    assertEquals(Option.of(5), pi.next())
    assertEquals(Option.of(7), pi.next())
    assertEquals(Option.of(9), pi.next())
    assertEquals(Option.of(11), pi.next())
    assertEquals(Cons(5, Cons(7, Cons(9, Cons(11, Nil())))), pi.allSoFar()) // elementi già prodotti
    for (_ <- 0 until 10) {
      pi.next(); // procedo in avanti per un po'..
    }
    assertEquals(Option.of(33), pi.next()); // sono arrivato a 33
  }

  @Test
  def testRandom(): Unit = {
    val pi: PowerIterator[Boolean] = factory.randomBooleans(4)
    val b1 = Option.getOrElse(pi.next(), None)
    val b2 = Option.getOrElse(pi.next(), None)
    val b3 = Option.getOrElse(pi.next(), None)
    val b4 = Option.getOrElse(pi.next(), None)
    assertTrue(pi.next() == Option.empty)
    assertEquals(List(b1, b2, b3, b4), pi.allSoFar())
  }

  @Test
  def testFromList(): Unit = {
    val pi = factory.fromList(List("a", "b", "c"))
    assertEquals(pi.next(), Option.of("a"))
    assertEquals(pi.next(), Option.of("b"))
    assertEquals(pi.allSoFar(), List("a", "b"))
    assertEquals(pi.next(), Option.of("c"))
    assertEquals(pi.allSoFar(), List("a", "b", "c"))
    assertEquals(Option.empty, pi.next())
  }

  @Test
  def optionalTestReservedOnList(): Unit = {
    val pi = factory.fromList(List("a", "b", "c"))
    assertEquals(pi.next(), Option.of("a"))
    assertEquals(pi.next(), Option.of("b"))
    val pi2 = pi.reversed()
    assertEquals(pi.next(), Option.of("c"))
    assertEquals(Option.empty, pi.next())
    assertEquals(pi2.next(), Option.of("b"))
    assertEquals(pi2.next(), Option.of("a"))
    assertEquals(pi2.allSoFar(), List("b", "a"))
    assertEquals(Option.empty, pi2.next())
  }

  @Test
  def optionalTestReversedOnIncremental(): Unit = {
    val pi = factory.incremental(0, x => x + 1)
    assertEquals(Option.of(0), pi.next())
    assertEquals(Option.of(1), pi.next())
    assertEquals(Option.of(2), pi.next())
    assertEquals(Option.of(3), pi.next())
    val pi2 = pi.reversed()
    assertEquals(Option.of(3), pi2.next())
    assertEquals(Option.of(2), pi2.next())
    val pi3 = pi2.reversed()
    assertEquals(Option.of(2), pi3.next())
    assertEquals(Option.of(3), pi3.next())
    assertEquals(Option.empty, pi3.next())
  }

}