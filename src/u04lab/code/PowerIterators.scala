package u04lab.code

import Optionals._
import Lists._
import Lists.List._
import Streams.Stream

import scala.util.Random

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

trait PowerIteratorsFactory {
  object PowerIterator {
    def apply[A](stream: Stream[A]): PowerIterator[A] = PowerIteratorImpl(stream)
    private case class PowerIteratorImpl[A](var stream: Stream[A]) extends PowerIterator[A] {
      private var pastList: List[A] = Nil()
      override def next(): Option[A] = stream match {
        case Stream.Cons(h, t) =>
          stream = t()
          pastList = List.Cons(h(), pastList)
          Option.of(h())
        case _ => Option.empty
      }
      override def allSoFar(): List[A] = List.reverse(pastList)
      override def reversed(): PowerIterator[A] = PowerIteratorImpl(List.toStream(pastList))
    }
  }
  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]): PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {
  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = PowerIterator(Stream.iterate(start)(successive))
  override def fromList[A](list: List[A]): PowerIterator[A] = PowerIterator(List.toStream(list))
  override def randomBooleans(size: Int): PowerIterator[Boolean] = PowerIterator(Stream.take(Stream.generate(Random.nextBoolean))(size))
}
