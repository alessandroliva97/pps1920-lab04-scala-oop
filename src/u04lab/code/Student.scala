package u04lab.code

import Lists._
import Lists.List._

trait Student {
  def name: String
  def year: Int
  def enrolling(courses: Course*): Unit // the student participates to a Course
  def courses: List[String] // names of course the student participates to
  def hasTeacher(teacher: String): Boolean // is the student participating to a course of this teacher?
}

trait Course {
  def name: String
  def teacher: String
}

object Student {
  def apply(name: String, year: Int = 2017): Student = StudentImpl(name, year)
  case class StudentImpl(name: String, year: Int = 2017) extends Student {
    private var courseList: List[Course] = Nil()
    def enrolling(courses: Course*): Unit = for (c <- courses) courseList = append(courseList, Cons(c, Nil()))
    def courses: List[String] = map(courseList)(_.name)
    def hasTeacher(teacher: String): Boolean = contains(map(courseList)(_.teacher))(teacher)
  }
}

object Course {
  def apply(name: String, teacher: String): Course = CourseImpl(name, teacher)
  case class CourseImpl(name: String, teacher: String) extends Course
}

object sameTeacher {
  def unapply(list: List[Course]): Option[String] = list match {
    case Cons(h, Nil()) => Some(h.teacher)
    case Cons(h, sameTeacher(t)) if t == h.teacher => Some(t)
    case _ => Option.empty
  }
}

object Try extends App {
  val cPPS = Course("PPS", "Viroli")
  val cPCD = Course("PCD", "Ricci")
  val cSDR = Course("SDR", "D'Angelo")
  val s1 = Student("mario", 2015)
  val s2 = Student("gino", 2016)
  val s3 = Student("rino") //defaults to 2017
  s1.enrolling(cPPS)
  s1.enrolling(cPCD)
  s2.enrolling(cPPS)
  s3.enrolling(cPPS)
  s3.enrolling(cPCD)
  s3.enrolling(cSDR)
  println(s1.courses, s2.courses, s3.courses) // (Cons(PCD,Cons(PPS,Nil())),Cons(PPS,Nil()),Cons(SDR,Cons(PCD,Cons(PPS,Nil()))))
  println(s1.hasTeacher("Ricci")) // true
  println(List(cPPS, cPCD) match {
    case sameTeacher(_) => true
    case _ => false
  }) // false
  val cOOP = Course("OOP", "Viroli")
  println(List(cPPS, cOOP) match {
    case sameTeacher(_) => true
    case _ => false
  }) // true
}
